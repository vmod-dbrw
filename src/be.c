/* This file is part of vmod-dbrw
   Copyright (C) 2013-2014 Sergey Poznyakoff

   Vmod-dbrw is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-dbrw is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-dbrw.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "dbrw.h"

static struct dbrw_backend *bcktab[] = {
#ifdef USE_SQL_MYSQL
	&mysql_backend,
#endif
#ifdef USE_SQL_PGSQL
	&pgsql_backend,
#endif
	NULL
};

struct dbrw_backend *
dbrw_backend_select(const char *name)
{
	int i;

	for (i = 0; bcktab[i]; i++) {
		if (strcmp(bcktab[i]->name, name) == 0)
			return bcktab[i];
	}
	return NULL;
}

char *
findparam(char **params, char *name)
{
	char *p, *q;

	while (*params) {
		p = *params++;
		for (q = name; *p && *q && *p == *q; p++, q++);
		if (*q == 0 && *p == '=')
			return p+1;
	}
	return NULL;
}

