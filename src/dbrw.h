/* This file is part of vmod-dbrw
   Copyright (C) 2013-2020 Sergey Poznyakoff

   Vmod-dbrw is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-dbrw is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-dbrw.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <regex.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vcc_if.h"

#ifdef VPFX
# define VEVENT(a) VPFX(a)
#else
/* For compatibility with varnish prior to 6.2 */
# define VEVENT(a) a
#endif

struct dbrw_connection;

struct dbrw_backend {
	char *name;
	
	int (*sql_init) (struct dbrw_connection *);
	void (*sql_destroy) (struct dbrw_connection *);
	int (*sql_connect) (struct dbrw_connection *);
	int (*sql_disconnect) (struct dbrw_connection *);
	char *(*sql_escape) (struct dbrw_connection *, const char *);
	int (*sql_query) (struct dbrw_connection *, const char *);
	unsigned (*sql_num_tuples) (struct dbrw_connection *);
	unsigned (*sql_num_fields) (struct dbrw_connection *);
	int (*sql_free_result) (struct dbrw_connection *);
	const char *(*sql_get_column)(struct dbrw_connection *, size_t, size_t);
	int (*sql_idle_timeout)(struct dbrw_connection *);
};

enum {
	state_init,
	state_connected,
	state_result,
	state_error,
	state_disabled
};

#define HTTP_STATUS_LEN 3

struct dbrw_config {
	unsigned magic;
#define DBRW_CONFIG_MAGIC 0x67636667
	int debug_level;
	struct dbrw_backend *backend;
	char **param;
	char *param_str;
	char *query;
	int qdisp;
	int regflags;
	int match_type;
	char status[HTTP_STATUS_LEN+1];
	int idle_timeout;
	VTAILQ_ENTRY(dbrw_config) list;
};

struct dbrw_connection {
	unsigned magic;
#define DBRW_CONNECTION_MAGIC 0x6773716c
	int state;                 /* Connection state */
	int busy:1;
	struct dbrw_config *conf;  /* Pointer to the configuration data */
	regmatch_t *matches;       /* Match map */
	size_t matchsize;          /* Total number of entries in match map */
	time_t timestamp;          /* Last used at */           
	void *data;                /* Backend-specific data */
	VTAILQ_ENTRY(dbrw_connection) list;
};

void dbrw_debug(const char *fmt, ...);
void dbrw_error(const char *fmt, ...);

#define debug(c,n,a) do { if ((c)->debug_level>=(n)) dbrw_debug a; } while (0)

#ifdef USE_SQL_MYSQL
struct dbrw_backend mysql_backend;
#endif
#ifdef USE_SQL_PGSQL
struct dbrw_backend pgsql_backend;
#endif

struct dbrw_backend *dbrw_backend_select(const char *name);

int sql_init(struct dbrw_connection *);
int sql_connect(struct dbrw_connection *pd);
void sql_disconnect(struct dbrw_connection *pd);
char *sql_escape(struct dbrw_connection *pd, const char *input);
int sql_query(struct dbrw_connection *pd, const char *input);
unsigned sql_num_tuples(struct dbrw_connection *pd);
unsigned sql_num_fields(struct dbrw_connection *pd);
void sql_free_result(struct dbrw_connection *pd);
void sql_destroy(struct dbrw_connection *pd);
const char *sql_get_column(struct dbrw_connection *pd, unsigned row, unsigned col);
int sql_idle_timeout(struct dbrw_connection *conn);

char *findparam(char **params, char *name);

