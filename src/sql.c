/* This file is part of vmod-dbrw
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-dbrw is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-dbrw is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-dbrw.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "dbrw.h"
#include <assert.h>

#define CONN_ASSERT(conn) do {			\
	if (!(conn)->conf)			\
		return;				\
	if ((conn)->state == state_disabled)	\
		return;				\
	} while(0)

#define CONN_ASSERT_VAL(conn,v) do {		\
	if (!(conn)->conf)			\
		return v;			\
	if ((conn)->state == state_disabled)	\
		return v;			\
	} while(0)


int
sql_init(struct dbrw_connection *conn)
{
	CONN_ASSERT_VAL(conn, 1);
	debug(conn->conf, 3, ("sql_init called"));
	if (conn->conf->backend->sql_init)
		return conn->conf->backend->sql_init(conn);
	return 0;
}

void
sql_destroy(struct dbrw_connection *conn)
{
	CONN_ASSERT(conn);
	debug(conn->conf, 5, ("sql_destroy: state=%d", conn->state));
	do {
		switch (conn->state) {
		case state_init:
			break;
		case state_result:
			sql_free_result(conn);
			break;
		case state_connected:
			sql_disconnect(conn);
			break;
		default:
			/* Force destroy */
			conn->state = state_init;
		}
	} while (conn->state != state_init);
	if (conn->conf->backend->sql_destroy)
		return conn->conf->backend->sql_destroy(conn);
}

int
sql_connect(struct dbrw_connection *conn)
{
	CONN_ASSERT_VAL(conn, 1);
	debug(conn->conf, 5, ("sql_connect: state=%d", conn->state));
	switch (conn->state) {
	case state_init:
		break;
	case state_connected:
	case state_result:
		return 0;
	case state_error:
		sql_disconnect(conn);
		if (conn->state != state_init)
				return 1;
		break;
	case state_disabled:
		return 1;
	}
	assert(conn->conf->backend->sql_connect);
	if (conn->conf->backend->sql_connect(conn))
		return 1;
	debug(conn->conf, 5, ("sql_connect: success"));
	conn->state = state_connected;
	return 0;
}

void
sql_disconnect(struct dbrw_connection *conn)
{
	CONN_ASSERT(conn);
	debug(conn->conf, 5, ("sql_disconnect: state=%d", conn->state));
	assert(conn->conf->backend->sql_disconnect);
	if (conn->conf->backend->sql_disconnect(conn) == 0)
		conn->state = state_init;
}

char *
sql_escape(struct dbrw_connection *conn, const char *input)
{
	CONN_ASSERT_VAL(conn, NULL);
	debug(conn->conf, 5, ("sql_escape: state=%d; input=%s",
			      conn->state, input));
	if (conn->conf->backend->sql_escape)
		return conn->conf->backend->sql_escape(conn, input);
	return strdup(input);
}

int
sql_query(struct dbrw_connection *conn, const char *input)
{
	CONN_ASSERT_VAL(conn, 1);
	debug(conn->conf, 1, ("state=%d, query=%s", conn->state, input));
	do {
		switch (conn->state) {
		case state_init:
			if (sql_connect(conn) || conn->state != state_connected)
				return 1;
			break;

		case state_connected:
			break;

		case state_result:
			sql_free_result(conn);
			break;
		
		case state_error:
			sql_disconnect(conn);
			if (conn->state != state_init)
				return 1;
			break;

		default:
			return 1;
		}
	} while (conn->state != state_connected);
	conn->timestamp = time(NULL);
	if (conn->conf->backend->sql_query(conn, input) == 0) 
		return 0;
	return 1;
}

unsigned
sql_num_tuples(struct dbrw_connection *conn)
{
	CONN_ASSERT_VAL(conn, 0);
	assert(conn->conf->backend->sql_num_tuples);
	return conn->conf->backend->sql_num_tuples(conn);
}

unsigned
sql_num_fields(struct dbrw_connection *conn)
{
	CONN_ASSERT_VAL(conn, 0);
	assert(conn->conf->backend->sql_num_fields);
	return conn->conf->backend->sql_num_fields(conn);
}

void
sql_free_result(struct dbrw_connection *conn)
{
	CONN_ASSERT(conn);
	if (conn->conf->backend->sql_free_result) {
		if (conn->conf->backend->sql_free_result(conn))
			return;
	}
	conn->state = state_connected;
}

const char *
sql_get_column(struct dbrw_connection *conn, unsigned row, unsigned col)
{
	CONN_ASSERT_VAL(conn, NULL);
	assert(conn->conf->backend->sql_get_column);
	if (conn->state != state_result)
		return NULL;
	return conn->conf->backend->sql_get_column(conn, row, col);
}

int
sql_idle_timeout(struct dbrw_connection *conn)
{
	CONN_ASSERT_VAL(conn, -1);
	if (!conn->conf->backend->sql_idle_timeout)
		return -1;
	return conn->conf->backend->sql_idle_timeout(conn);
}
