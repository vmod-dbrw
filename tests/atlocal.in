# @configure_input@                                     -*- shell-script -*-
# Configurable variable values for vmod-dbrw test suite.
# Copyright (C) 2013-2017 Sergey Poznyakoff

PATH=@abs_builddir@:@abs_top_builddir@/src:@abs_top_srcdir@/build-aux:$top_srcdir:$srcdir:@VARNISH_SBINDIR@:$PATH
INITFILE=@abs_builddir@/.TESTINIT
FAILFILE=@abs_builddir@/FAILURE
VARNISHD=@VARNISHD@
VARNISHTEST="@VARNISHTEST@ -Dvarnishd=$VARNISHD"
VARNISHVERSION=@VARNISH_MAJOR@
: ${DBRW_TEST_DBTYPE=@DBRW_TEST_DBTYPE@}
: ${DBRW_TEST_PARAMS=@DBRW_TEST_PARAMS@}
: ${DBRW_TEST_SERVER=@DBRW_TEST_SERVER@}
: ${DBRW_TEST_DATABASE=@DBRW_TEST_DATABASE@}
: ${DBRW_TEST_USER=@DBRW_TEST_USER@}
: ${DBRW_TEST_PASS=@DBRW_TEST_PASS@}
: ${DBRW_TEST_DEBUG=@DBRW_TEST_DEBUG@}

if [ -n "$DBRW_TEST_SERVER" ]; then
    DBRW_TEST_PARAMS="$DBRW_TEST_PARAMS;server=$DBRW_TEST_SERVER"
fi
if [ -n "$DBRW_TEST_DATABASE" ]; then
    DBRW_TEST_PARAMS="$DBRW_TEST_PARAMS;database=$DBRW_TEST_DATABASE"
fi
if [ -n "$DBRW_TEST_USER" ]; then
    DBRW_TEST_PARAMS="$DBRW_TEST_PARAMS;user=$DBRW_TEST_USER"
fi
if [ -n "$DBRW_TEST_PASS" ]; then
    DBRW_TEST_PARAMS="$DBRW_TEST_PARAMS;password=$DBRW_TEST_PASS"
fi
if [ -n "$DBRW_TEST_DEBUG" ]; then
    DBRW_TEST_PARAMS="$DBRW_TEST_PARAMS;debug=$DBRW_TEST_DEBUG"
fi

at_vcl_backend() {
	cat <<EOT
varnishtest "$at_desc"

server s1 {
       rxreq
       txresp
} -start

varnish v1 -vcl+backend {
	import std;
        import dbrw from "$abs_top_builddir/src/.libs/libvmod_dbrw.so";

	sub vcl_recv {
		dbrw.config("$DBRW_TEST_DBTYPE", "$DBRW_TEST_PARAMS",
		            {"$1"});
		set req.http.X-Redirect-To =
			dbrw.rewrite("host=" + req.http.Host + ";" +
			             "url=" + req.url);
		if (req.http.X-Redirect-To != "") {
		        return(synth(301, "Redirect"));
		} else if (req.http.X-VMOD-DBRW-Error == "1") {
			return(synth(500, "DBRW Error"));
		}
	}

	sub vcl_synth {
		if (req.http.X-VMOD-DBRW-Error == "1") {
			set resp.http.X-VMOD-DBRW-Error = "1";
		}
		if (resp.status == 301) {
			if (req.http.X-VMOD-DBRW-Status) {
				set resp.status = std.integer(req.http.X-VMOD-DBRW-Status, 301);
			}
		        set resp.http.Location = req.http.X-Redirect-To;
			return (deliver);
		}
	}
} -start
EOT
}
