.\" This file is part of Vmod-dbrw -*- nroff -*-
.\" Copyright (C) 2013-2022 Sergey Poznyakoff
.\"
.\" Vmod-dbrw is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vmod-dbrw is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with vmod-dbrw.  If not, see <http://www.gnu.org/licenses/>.
.TH VMOD-DBRW 3 "August 21, 2022" "VMOD-DBRW" "User Reference"
.SH NAME
vmod-dbrw \- Database-driven rewrite rules for Varnish Cache
.SH SYNOPSIS
.B import dbrw;
.PP
.BI "VOID dbrw.config(STRING " dbtype ", STRING " params ", STRING " query ");"
.PP
.BI "STRING dbrw.rewrite(STRING " args ");"
.SH DESCRIPTION
.B Vmod-dbrw
is a Varnish Cache module implementing database-driven rewrite procedures.
Its intended use is for web sites that need an exceedingly big number
of redirect and/or rewrite rules.
.PP
The \fBdbrw.config\fR function should be called exactly once per HTTP session, 
before calling \fBdbrw.rewrite\fR (normally it is done from
\fBvcl_recv\fR).  It configures the module and supplies it the information 
necessary for accessing the database and interpreting the data
obtained from it.
.PP
The \fIdbtype\fR argument specifies the database type to use.
Valid values are \fBmysql\fR (for \fBMySQL\fR) and \fBpgsql\fR (for
\fBPostgreSQL\fR).
.PP
.B Vmod-dbrw
does not impose any requirements on the structure of the database,
therefore the connection arguments and access credentials may vary
depending on the flavor of the database used.  This information is
supplied in the \fIparams\fR argument, which is a list of
\fINAME\fB=\fIVALUE\fR assignments separated with semicolons.
The
\fIVALUE\fR can be any sequence of characters, excepting white space
and semicolon.  If any of these have to appear in it, they must either
be escaped by prepending them with a backslash, or entire \fIVALUE\fR
enclosed in a pair of (single or double) quotes.  The following
\fBescape sequences\fR are allowed for use in \fIVALUE\fR:
.PP
.nf
.ta 8n 18n 42n
.ul
	Sequence	Expansion	ASCII
	\fB\\\\\fR	\fB\\\fR	134
	\fB\\"\fR	\fB"\fR	042
	\fB\\a\fR	audible bell	007	
	\fB\\b\fR	backspace	010
	\fB\\f\fR	form-feed	014
	\fB\\n\fR	new line	012
	\fB\\r\fR	charriage return	015
	\fB\\t\fR	horizontal tabulation	011
	\fB\\v\fR	vertical tabulation	013
.fi
.sp
Any other character following a backslash is output verbatim.
.sp
The valid parameters are:
.TP
\fBdebug\fR=\fIN\fR
Set debugging level.  \fIN\fR is a decimal number.
.TP
\fBserver\fR=\fIHOST\fR
Name or IP address of the database server to connect to.  If not
defined, localhost (\fB127.0.0.1\fR) is assumed.  For \fBMySQL\fR
databases, if \fIHOST\fR begins with a slash (\fB/\fR), its value is
taken to be the full pathname of the local UNIX socket to connect to.
.TP
\fBport\fR=\fINUM\fR
Port number on the \fBserver\fR to connect to.  Default is \fB3306\fR
for \fBMySQL\fR and \fB5432\fR for \fBPostgres\fR.
.TP
\fBdatabase=\fINAME\fR
The database name.
.TP
\fBconfig\fR=\fIFILE\fR
(\fBMySQL\fR-specific) Read credentials from the \fBMySQL\fR options
file \fIFILE\fR.
.TP
\fBgroup\fR=\fINAME\fR
(\fBMySQL\fR-specific) Read credentials from section \fINAME\fR of the
options file supplied with the \fBconfig\fR parameter.  Default
section name is \fBclient\fR.
.TP
\fBcacert\fR=\fIFILE\fR
Use secure connection to the database server via SSL.  The \fIFILE\fR
is a full pathname to the certificate authority file.
.TP
\fBtimeout\fR=\fIN\fR
Sets idle timeout for a single connection (seconds).  The connection
will be closed and opened again if the module is to use it after
\fIN\fR or more seconds since its last use. Set \fBtimeout=-1\fR to
disable idle timeout (sessions will remain open until the SQL
server closes them). Set \fBtimeout=0\fR to close the connection after
each request (not recommended).

The default depends on the selected SQL backend.  For MySQL, it equals
the value of the \fBwait_timeout\fR global variable.  For PostgreSQL,
it is \fB-1\fR.
.TP
\fBoptions\fR=\fISTRING\fR
(\fBPostgres\fR-specific) Connection options.
.TP
\fBuser\fR=\fINAME\fR
Database user name.
.TP
\fBpassword\fR=\fISTRING\fR
Password to access the database.
.PP
Once configured, the \fBrewrite\fR function can be called in the
appropriate place of the Varnish configuration file.  Its argument
\fIargs\fR is a list of variable assignments separated with
semicolons, similarly to the \fIparams\fR argument described above.
Each assignment has the form \fINAME\fB=\fIVALUE\fR.  When called,
the function expands the \fBSQL\fR query supplied with the \fIquery\fR
argument to the \fBconfig\fR call, by replacing each \fB$\fINAME\fR
construct (a \fBvariable reference\fR) with the corresponding
\fIVALUE\fR from its argument.  Similarly to the shell syntax, the
variable reference can be written as \fB${\fINAME\fB}\fR.  This
form can be used in contexts where the variable name is immediately
followed by another letter, to prevent it from being counted as a part
of the name.
.PP
The special construct
.sp
.EX
$(urlprefixes \fIPATH\fR)
.EE
.sp
expands to a comma-separated list of all possible path prefixes in
\fIPATH\fR. Each element in the list is quoted, so the result can
safely be used in SQL statements. For example,
.sp
.EX
$(urlprefixes "/local/user/login")
.EE
.sp
produces
.sp
.EX
 '/local/user/login','/local/user','/local'
.EE
.PP
This statement is usually used in \fBIN\fR SQL constructs, e.g.
.sp
.EX
SELECT * FROM table WHERE url IN ($(urlprefixes $url))
.EE
.PP
The expanded query is then sent to the database server.  Handling
of the return value depends on the number of fields it contains.
.SS Strict matches
If the returned set consists of one or two columns, only the
first tuple is used and the value of its first column is returned.
The second column (if present) is ignored.
.SS Regexp matches
Otherwise, if the returned set consists of three or four columns, the
columns are interpreted as follows: \fBresult\fR, \fBregexp\fR,
\fBvalue\fR and optional \fBflags\fR.  For each returned tuple, the
\fBvalue\fR column undergoes variable expansion, using the same
algorithm as when preparing the query, and the resulting string is
matched with the \fBregexp\fR column, which is treated as an extended
\fBPOSIX\fR regular expression.  If the match occurs, the \fBresult\fR
column is expanded by replacing \fBbackreferences\fR.  Each occurrence
of \fB$\fIDIGIT\fR (where \fIDIGIT\fR stands for a decimal digit from
\fB0\fR through \fB9\fR) is replaced with the contents of the
\fIDIGIT\fR's parenthesized subexpression in \fBregexp\fR.  For
compatibility with the traditional usage, the \fB\\\fIDIGIT\fR
notation is also allowed.  The resulting value is returned.
.PP
Optional \fBflags\fR column is a comma-separated list of flags that
modify regular expression handling.  The following flags are defined:
.TP
.BR NC " or " nocase
Treat \fBregexp\fR as case-insensitive.
.TP
.B case
Treat \fBregexp\fR as case-sensitive (default).
.TP
.B eq
Use exact string matching.
.TP
.BR QSA " or " qsappend
Treat the resulting value as URL; append any query string from the
original \fBvalue\fR to it.
.TP
.BR QSD " or " qsdiscard
Treat the resulting value as URL; discard any query string attached to
the original \fBvalue\fR.
.TP
\fBredirect=\fICODE\fR or \fBR=\fICODE\fR
On success, set the \fBX\-VMOD\-DBRW\-Status\fR header to \fICODE\fR,
which must be a valid HTTP status code.
.TP
.B regex
Use regular expression matching.  This is the default.  This flag is
provided for completeness sake, as a counterpart of
.BR eq .
.PP
If \fBregexp\fR or \fBvalue\fR is NULL, the tuple is handled as
described in
.BR "Strict matches" .
.PP
If \fBflags\fR is NULL, it is ignored.
.SH HTTP HEADERS
Upon return, 
.B dbrw.return
may set one of the following headers in
.BR resp.http :
.TP
.B X\-VMOD\-DBRW\-Status
If the \fBredirect\fR flag was used, this header contains the HTTP
response code to be used instead of the default.
.TP
.B X\-VMOD\-DBRW\-Error
This header is set to \fB1\fR if an error occurred during the rewrite.
.SH EXAMPLES
The examples in this section assume \fBMySQL\fR databases.  Any
details not related to \fBvmod-dbrw\fR are omitted.
.SS Redirects
.PP
This example shows how to implement apache-style permanent redirects
in Varnish.
.PP
The following table is used to keep the redirection data:
.PP
.EX
CREATE TABLE redirects (
    host varchar(255) NOT NULL DEFAULT '',
    url varchar(255) NOT NULL DEFAULT '',
    dest varchar(255) DEFAULT NULL,
    PRIMARY KEY (host,url)
);
.EE
.PP
The VCL code:
.PP
.EX
sub vcl_recv {
    dbrw.config("mysql", "database=dbname;user=varnish",
                {"SELECT dest
                    FROM redirects
                   WHERE host='$host'
                     AND url='$url'"});
    set req.http.X-Redirect-To =
        dbrw.rewrite("host=" + req.http.Host + ";" +
                     "url=" + req.url);
    if (req.http.X-Redirect-To != "") {
        return(synth(301, "Redirect"));
    }
}

sub vcl_synth {
    if (resp.status == 301) {
        set resp.http.Location = req.http.X-Redirect-To;
        return (deliver);
    }
}
.EE
.SS Rewrites
.PP
The database structure is as follows:
.PP
.EX
CREATE TABLE rewrite (
    host varchar(255) NOT NULL DEFAULT '',
    url varchar(255) NOT NULL DEFAULT '',
    dest varchar(255) DEFAULT NULL,
    value varchar(255) DEFAULT NULL,
    pattern varchar(255) DEFAULT NULL,
    flags char(64) DEFAULT NULL,
    KEY source (host,url)
)
.EE
.PP
The VCL:
.PP
.EX
sub vcl_recv {
    # It is supposed that the url column contains an SQL-style
    # wildcard pattern.
    dbrw.config("mysql", "database=varnish;user=varnish;debug=10",
                {"SELECT dest,pattern,value,flags FROM rewrite
                  WHERE host='$host'
		    AND url IN ($(urlprefixes $url))
	          ORDER BY LENGTH(dest),value DESC"});
    set req.http.X-Redirect-To =
        dbrw.rewrite("host=" + req.http.Host + ";" +
                     "url=" + req.url);
    if (req.http.X-Redirect-To != "") {
        return(synth(750, "Redirect"));
    }
}

sub vcl_synth {
    if (resp.status == 750) {
        set obj.status = std.integer(req.http.X-VMOD-DBRW-Status, 301);
        set resp.http.Location = req.http.X-Redirect-To;
        return (deliver);
    }
}
.EE
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.	ds package vmod-dbrw
.	ds version 2.8
.	so download.inc
\}
.SH "SEE ALSO"
.BR vcl (7),
.BR varnishd (1).
.PP
.ie "\V[MANCGI]"WEBDOC" \{\
Complete documentation for
.B vmod-dbrw
in various formats is
.URL http://puszcza.gnu.org.ua/software/vmod-dbrw/manual.html "available online" .
\}
.el \{\
The full documentation for
.B vmod-dbrw
is maintained as a Texinfo
manual.  If the
.B info
program and
.B vmod-dbrw
module are properly installed at your site, the  command
.PP
.nf
.in +4
.B info vmod-dbrw
.in
.fi
.PP
should give you access to the complete manual.
\}
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2013-2022 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
